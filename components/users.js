'use strict';

class Users {

    constructor(params) {
        if (params) {
            this.option = params.option || 'default param';
            this.value = params.value || 'default param';
        }
    }

    echo(callback) {

        let returnobj = {
            message: 'Go Serverless v1.0! Linus\'s first serverless!',
        };

        let params = {};

        if (this.option) {
            params.option = this.option;
        }

        if (this.value) {
            params.value = this.value;
        }

        returnobj.params = params;

        let response = {
            statusCode: 200,
            body: JSON.stringify(returnobj)
        };

        callback(null, response);
    }

}


module.exports = Users;